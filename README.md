# CustomDefault

[![Build](https://gitlab.com/Douman/custom_default/badges/master/pipeline.svg)](https://gitlab.com/Douman/custom_default/pipelines)
[![Crates.io](https://img.shields.io/crates/v/cute_custom_default.svg)](https://crates.io/crates/cute_custom_default)
[![Documentation](https://docs.rs/cute_custom_default/badge.svg)](https://docs.rs/crate/cute_custom_default/)

Derive macro for `Default` trait with customization
